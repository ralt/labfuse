# labfuse

TODO

## Roadmap

- Implement the rest of the fuse methods
- Implement a custom .git/ passthru that handles .git/index separately for each tree
- Review the 3-phase git fetch
- Implement the tree-gitlab-api methods (find a big git repo)
- Implement the commits/ and pipelines/ subcomponents
- Implement custom struct stat{} data, e.g. mtime
