(defsystem "labfuse"
  :author "Florian Margaine <florian@margaine.com>"
  :license "GPLv2"
  :defsystem-depends-on ("wild-package-inferred-system" "cffi-grovel")
  :class "winfer:wild-package-inferred-system"
  :around-compile (lambda (next)
                    (proclaim '(optimize
                                (debug 3)
                                (safety 1)
                                (speed 3)))
                    (funcall next))
  :depends-on ("labfuse/server/*")
  :build-operation :static-program-op
  :build-pathname "labfuse"
  :entry-point "labfuse/server/main:main")
