(uiop:define-package :labfuse/server/groups
    (:use :cl :labfuse/server/gitlab :labfuse/server/projects)
  (:import-from :alexandria #:hash-table-keys)
  (:import-from :cl-ppcre #:split)
  (:import-from :jsown #:val)
  (:import-from :quri #:uri)
  (:import-from :uiop #:getenv)
  (:export #:*groups*
	   #:*projects*
	   #:populate-groups
	   #:list-group-projects))

(in-package :labfuse/server/groups)

(defvar *groups* (make-hash-table :test #'equal))
(defvar *projects* (make-hash-table :test #'equal))

(defun populate-groups (hierarchy &optional url token user)
  (setf *gitlab-uri* (uri (or url (getenv "GITLAB_URL") "https://gitlab.com/")))
  (setf *gitlab-token* (or token (getenv "GITLAB_PRIVATE_TOKEN")))
  (setf *gitlab-user* (or user (getenv "GITLAB_USER")))

  (gitlab-request ("groups")
      (groups)
    (dolist (group-object groups)
      (let* ((full-path (val group-object "full_path"))
	     (split-path (split "/" full-path))
	     (group-id (val group-object "id")))
	(add-split-path-to-groups split-path hierarchy)
	(setf (gethash full-path *groups*) group-id)

	(gitlab-request (`("groups" ,group-id "subgroups"))
	    (subgroups)
	  (dolist (subgroup-object subgroups)
	    (let* ((full-path (val subgroup-object "full_path"))
		   (split-path (split "/" full-path))
		   (subgroup-id (val subgroup-object "id")))
	      (add-split-path-to-groups split-path hierarchy)
	      (setf (gethash full-path *groups*) subgroup-id))))))))

(defun add-split-path-to-groups (split-path hierarchy)
  (let* ((first (first split-path))
	 (rest (rest split-path))
	 (hash-table (multiple-value-bind (existing-hash-table presentp)
			 (gethash first hierarchy)
		       (if presentp
			   existing-hash-table
			   (setf (gethash first hierarchy) (make-hash-table :test #'equal))))))
    (when rest
      (add-split-path-to-groups rest hash-table))))

(defun list-group-projects (hierarchy merged-path split-path group-id)
  (let ((group-hash-table (get-group-hash-table hierarchy split-path)))
    (when (= (hash-table-count group-hash-table) 0)
      (gitlab-request (`("groups" ,group-id "projects"))
	  (projects)
	(dolist (project projects)
	  (let ((project-path (val project "path"))
		(project-object (make-instance
				 'project
				 :id (val project "id")
				 :path-with-namespace (val project "path_with_namespace")
				 :http-url-to-repo (val project "http_url_to_repo"))))
	    (setf (gethash project-path group-hash-table) project-object)
	    (setf (gethash (format nil "~a/~a" merged-path project-path) *projects*)
		  project-object)))))

    (hash-table-keys group-hash-table)))

(defun get-group-hash-table (hierarchy split-path)
  (let ((table (gethash (first split-path) hierarchy))
	(rest (rest split-path)))
    (if (not rest)
	table
	(get-group-hash-table table rest))))
