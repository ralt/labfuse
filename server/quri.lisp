(uiop:define-package :labfuse/server/quri
    (:use :cl)
  (:import-from :quri
		#:uri
		#:uri-ftp-p
		#:uri-scheme
		#:uri-authority
		#:uri-path
		#:uri-ftp-typecode
		#:uri-query
		#:uri-fragment
		#:uri-file-p)
  (:export #:render-uri
	   #:uri-parsed-path))

(in-package :labfuse/server/quri)

;;;; A couple of fixes on top of quri.

(defmethod uri-parsed-path ((uri uri))
  (let* ((path (uri-path uri))
         (directory (pathname-directory path))
         (name (pathname-name path)))
    (if name
        (append directory (list name))
        directory)))

(defmethod (setf uri-parsed-path) (new (uri uri))
  (setf (uri-path uri) (namestring (make-pathname :directory new))))

(defmethod render-uri (uri &optional stream)
  "Additional fix: don't randomly lowercase things."
  (cond
    ((uri-ftp-p uri)
     (format stream
             "~@[~(~A~):~]~@[//~A~]~@[~A~]~@[;type=~A~]~@[?~A~]~@[#~A~]"
             (uri-scheme uri)
             (uri-authority uri)
             (uri-path uri)
             (uri-ftp-typecode uri)
             (uri-query uri)
             (uri-fragment uri)))
    ((uri-file-p uri)
     (format stream
             "~@[~(~A~)://~]~@[~a~]"
             (uri-scheme uri)
             (uri-path uri)))
    (t
     (format stream
             "~@[~(~A~):~]~@[//~A~]~@[~A~]~@[?~A~]~@[#~A~]"
             (uri-scheme uri)
             (uri-authority uri)
             (uri-path uri)
             (uri-query uri)
             (uri-fragment uri)))))
