(uiop:define-package :labfuse/server/gitlab
    (:use :cl #:labfuse/server/quri)
  (:import-from :drakma
                #:http-request
                #:*text-content-types*
                #:*drakma-default-external-format*)
  (:import-from :jsown #:parse #:val)
  (:import-from :quri
		#:uri
		#:uri-path
		#:copy-uri
		#:uri-query-params
		#:uri-userinfo)
  (:export #:uri-parsed-path
           #:gitlab-request
	   #:gitlab-project
	   #:gitlab-project-clone-uri
           #:*gitlab-uri*
           #:*gitlab-token*
	   #:*gitlab-user*))

(in-package :labfuse/server/gitlab)

(defvar *gitlab-uri* nil "Gitlab instance URI.")
(defvar *gitlab-token* nil "Gitlab private token.")
(defvar *gitlab-user* nil "Gitlab user.")

(push (cons "application" "json") *text-content-types*)
(setf *drakma-default-external-format* :utf8)

(defun %gitlab-request (endpoint eof-value &optional (page 1) raw)
  (let ((is-done nil))
    (lambda ()
      (block inner
        (when is-done (return-from inner eof-value))
        (multiple-value-bind (response status-code headers)
            (http-request
             (render-uri (let ((gitlab-uri (copy-uri *gitlab-uri*)))
                           (setf (uri-parsed-path gitlab-uri)
                                 `(:absolute "api" "v4" ,(if (listp endpoint)
                                                             (format nil "~{~a~^/~}"
                                                                     endpoint)
                                                             endpoint)))
                           (setf (uri-query-params gitlab-uri)
                                 `(("per_page" . "100")
                                   ("page" . ,page)))

                           gitlab-uri))
             :additional-headers `(("Private-Token" . ,*gitlab-token*)))
          (unless (= (the fixnum status-code) 200)
            (error (format nil "Invalid status code: ~a" status-code)))
          (when raw (return-from inner response))
          (incf (the fixnum page))
          (let ((result (parse response)))
            (when (or (null (assoc :x-next-page headers))
                      (string= (the string (cdr (assoc :x-next-page headers))) ""))
              (setf is-done t))
            result))))))

(defmacro gitlab-request ((endpoint &optional (page 1) raw) (results) &body body)
  (let ((generator (gensym))
        (eof (gensym)))
    `(let ((,generator (%gitlab-request ,endpoint ',eof ,page ,raw)))
       (loop
         (let ((,results (funcall ,generator)))
           (when (eql ,results ',eof)
             (return))
           ,@body)))))

(defun gitlab-project-clone-uri (http-url-to-repo)
  (let ((new-uri (uri http-url-to-repo)))
    (setf (uri-userinfo new-uri) (format nil "~a:~a" *gitlab-user* *gitlab-token*))
    new-uri))
