(uiop:define-package :labfuse/server/main
    (:use :cl :labfuse/server/groups :labfuse/server/projects)
  (:import-from :alexandria #:hash-table-keys)
  (:import-from :cl-fuse #:fuse-run)
  (:import-from :sb-thread
		#:make-mutex
		#:with-mutex
		#:make-waitqueue
		#:condition-wait
		#:condition-broadcast
		#:make-thread)
  (:export #:main))

(in-package :labfuse/server/main)

(defvar *groups-lock* (make-mutex :name "Groups lock")
  "Protects the following variables")
(defvar *groups-ready* nil)
(defvar *groups-ready-waitqueue* (make-waitqueue :name "Groups ready waitqueue"))

(defvar *hierarchy* (make-hash-table :test #'equal))

(defun directory-content (path)
  (with-mutex (*groups-lock*)
    (loop
      (if *groups-ready*
	  (return)
	  (condition-wait *groups-ready-waitqueue* *groups-lock*))))

  (when (= (length path) 0)
    (return-from directory-content (hash-table-keys *hierarchy*)))

  ;; groups and projects folders are special because we want them to
  ;; fetch lazily. IOW the idea is that for those top-folders, we're
  ;; fetching the list of projects, or the project's git packs, only
  ;; after it has been requested, in order to not download the whole
  ;; gitlab.
  (let ((merged-path (format nil "~{~a~^/~}" path)))
    (multiple-value-bind (group-id presentp)
	(gethash merged-path *groups*)
      (when presentp
	(return-from directory-content
	  (with-mutex (*groups-lock*)
	    (list-group-projects *hierarchy* merged-path path group-id)))))

    (multiple-value-bind (project presentp)
	(gethash merged-path *projects*)
      (when presentp
	(return-from directory-content (list-project-directory project nil)))))

  (multiple-value-bind (value rest presentp)
      (find-project path *hierarchy*)
    (when presentp
      (if (typep value 'project)
	  (list-project-directory value rest)
	  (hash-table-keys value)))))

(defun directoryp (path)
  (when (= (length path) 0)
    (return-from directoryp t))

  (let ((merged-path (format nil "~{~a~^/~}" path)))
    (multiple-value-bind (unused presentp)
	(gethash merged-path *groups*)
      (declare (ignore unused))
      (when presentp
	(return-from directoryp t)))

    (multiple-value-bind (unused presentp)
	(gethash merged-path *projects*)
      (declare (ignore unused))
      (when presentp
	(return-from directoryp t))))

  (multiple-value-bind (project rest presentp)
      (find-project path *hierarchy*)
    (when presentp
      (if project
	  (project-directoryp project rest)
	  t))))

(defun find-project (path hierarchy)
  (multiple-value-bind (value presentp)
      (gethash (first path) hierarchy)
    (unless presentp (return-from find-project nil))

    (if (typep value 'project)
	(values value (rest path) t)
	(if (null (rest path))
	    (values nil nil t)
	    (find-project (rest path) value)))))

(defun symlink-target (path)
  (multiple-value-bind (project rest presentp)
      (find-project path *hierarchy*)
    (when (and rest presentp)
      (project-symlink-target project rest))))

(defun symlinkp (path)
  (multiple-value-bind (project rest presentp)
      (find-project path *hierarchy*)
    (when (and rest presentp)
      (project-symlinkp project rest))))

(defun file-open (path flags)
  (multiple-value-bind (project rest presentp)
      (find-project path *hierarchy*)
    (when (and rest presentp)
      (project-file-open project rest flags))))

(defun file-release (path fh)
  (multiple-value-bind (project rest presentp)
      (find-project path *hierarchy*)
    (when (and rest presentp)
      (project-file-release project rest fh))))

(defun file-flush (path fh)
  (multiple-value-bind (project rest presentp)
      (find-project path *hierarchy*)
    (when (and rest presentp)
      (project-file-release project rest fh))))

(defun file-read (path size offset fh)
  (multiple-value-bind (project rest presentp)
      (find-project path *hierarchy*)
    (when (and rest presentp)
      (project-file-read project rest size offset fh))))

(defun file-size (path)
  (multiple-value-bind (project rest presentp)
      (find-project path *hierarchy*)
    (when (and rest presentp)
      (project-file-size project rest))))

(defun file-write (path data offset fh)
  (multiple-value-bind (project rest presentp)
      (find-project path *hierarchy*)
    (when (and rest presentp)
      (project-file-write project rest data offset fh))))

(defun file-write-whole (path data fh)
  (multiple-value-bind (project rest presentp)
      (find-project path *hierarchy*)
    (when (and rest presentp)
      (project-file-write project rest data 0 fh))))

(defun file-writeable-p (path)
  (multiple-value-bind (project rest presentp)
      (find-project path *hierarchy*)
    (when (and rest presentp)
      (project-file-writeable-p project rest))))

(defun file-executable-p (path)
  (multiple-value-bind (project rest presentp)
      (find-project path *hierarchy*)
    (when (and rest presentp)
      (project-file-writeable-p project rest))))

(defun chmod (path mode))

(defun chown (path uid gid))

(defun file-truncate (path offset))

(defun mkdir (path mode))

(defun unlink (path))

(defun rmdir (path))

(defun symlink (path content))

(defun rename (path target))

(defun main (&optional folder)
  (make-thread (lambda ()
		 (with-mutex (*groups-lock*)
		   (populate-groups *hierarchy*)
		   (setf *groups-ready* t)
		   (condition-broadcast *groups-ready-waitqueue*)))
	       :name "Groups population")
  (fuse-run `("labfuse"
	      ,(or folder (second uiop:*command-line-arguments*))
	      "-odebug")
	    :directory-content 'directory-content
	    :directoryp 'directoryp
	    :symlink-target 'symlink-target
	    :symlinkp 'symlinkp
	    :file-open 'file-open
	    :file-release 'file-release
	    :file-read 'file-read
	    :file-size 'file-size
	    :file-write 'file-write
	    :file-write-whole 'file-write-whole
	    :file-create nil
	    :file-writeable-p 'file-writeable-p
	    :file-executable-p 'file-executable-p
	    :chmod 'chmod
	    :chown 'chown
	    :truncate 'file-truncate
	    :file-flush 'file-flush
	    :mkdir 'mkdir
	    :unlink 'unlink
	    :rmdir 'rmdir
	    :symlink 'symlink
	    :rename 'rename
	    :call-manager (lambda (f &rest x)
			    (declare (ignore x))
			    (make-thread f))))
