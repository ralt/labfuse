(uiop:define-package :labfuse/server/projects
    (:use
     :cl
     :labfuse/server/gitlab
     :labfuse/server/quri
     :labfuse/server/utils)
  (:import-from :cl-ppcre #:split)
  (:import-from :jsown #:val)
  (:import-from :quri #:url-encode #:url-decode)
  (:import-from :sb-thread
                #:make-thread
                #:make-mutex
		#:make-waitqueue
                #:with-mutex
		#:condition-wait
		#:condition-broadcast)
  (:import-from :uiop #:run-program #:xdg-data-home #:getcwd #:chdir)
  (:export #:list-project-directory
	   #:project-directoryp
	   #:project
	   #:project-file-open
	   #:project-file-release
	   #:project-file-read
	   #:project-file-write
	   #:project-file-size
	   #:project-symlink-target
	   #:project-symlinkp
	   #:project-file-writeable-p
	   #:project-file-executable-p))

(in-package :labfuse/server/projects)

(defvar *project-top-level*
  '("commits"
    "tree"))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defvar *not-fetched* 0)
  (defvar *branches-fetched* 1)
  (defvar *first-pack-fetched* 2)
  (defvar *all-packs-fetched* 3))

(deftype fetch-status ()
  `(member ,*not-fetched* ,*branches-fetched* ,*first-pack-fetched* ,*all-packs-fetched*))

(defun safer-ensure-directories-exist (pathspec)
  (let ((counter 5))
    (loop
      (handler-case
	  (ensure-directories-exist pathspec)
	(error (e)
	  (when (= counter 0)
	    (error e))
	  (decf counter)
	  (sleep 0.1))))))

(defclass project ()
  ((lock :reader lock)
   (fetch-thread :accessor fetch-thread)
   (fetch-status-lock-1 :reader fetch-status-lock-1)
   (fetch-status-lock-2 :reader fetch-status-lock-2)
   (fetch-status-lock-3 :reader fetch-status-lock-3)
   (fetch-status-waitqueue-1 :reader fetch-status-waitqueue-1)
   (fetch-status-waitqueue-2 :reader fetch-status-waitqueue-2)
   (fetch-status-waitqueue-3 :reader fetch-status-waitqueue-3)
   (fetch-status :initform *not-fetched* :type fetch-status :accessor fetch-status)

   (id :initarg :id :reader project-id)
   (path-with-namespace :initarg :path-with-namespace :reader project-path-with-namespace)
   (http-url-to-repo :initarg :http-url-to-repo :reader project-http-url-to-repo)

   (heads :initform nil :accessor project-heads)

   (commits)
   (tree)))

(defclass project-commits ()
  ())

(defclass project-tree ()
  ((lock :reader lock)
   (thread :accessor thread)
   (queue :reader queue)
   (checked-out :accessor checked-out :initform nil)))

(defmethod initialize-instance :after ((tree project-tree) &key)
  (with-slots (lock queue) tree
    (setf lock (make-mutex))
    (setf queue (make-waitqueue))))

(defmethod initialize-instance :after ((p project) &key)
  (with-slots (lock
	       fetch-status-lock-1
	       fetch-status-lock-2
	       fetch-status-lock-3
	       fetch-status-waitqueue-1
	       fetch-status-waitqueue-2
	       fetch-status-waitqueue-3
	       id
	       commits
	       tree)
      p
    (setf lock (make-mutex :name (format nil "Project ~a lock" id)))
    (setf fetch-status-lock-1
	  (make-mutex :name (format nil "Project ~a fetch status lock 1" id)))
    (setf fetch-status-lock-2
	  (make-mutex :name (format nil "Project ~a fetch status lock 2" id)))
    (setf fetch-status-lock-3
	  (make-mutex :name (format nil "Project ~a fetch status lock 3" id)))
    (setf fetch-status-waitqueue-1
	  (make-waitqueue
	   :name (format nil "Project ~a fetch status waitqueue 1" id)))
    (setf fetch-status-waitqueue-2
	  (make-waitqueue
	   :name (format nil "Project ~a fetch status waitqueue 2" id)))
    (setf fetch-status-waitqueue-3
	  (make-waitqueue
	   :name (format nil "Project ~a fetch status waitqueue 3" id)))

    (setf commits (make-instance 'project-commits))
    (setf tree (make-instance 'project-tree))))

(defmethod list-project-directory ((p project) split-path)
  (with-mutex ((lock p))
    (unless (slot-boundp p 'fetch-thread)
      (setf (fetch-thread p)
	    (make-thread
	     (lambda ()
	       (fetch-git-repository p))
	     :name (format nil "Project ~a fetch git repository thread" (project-id p))))))

  (unless split-path
    (return-from list-project-directory
      *project-top-level*))

  (list-directory (pick-subcomponent p split-path) p (rest split-path)))

(defmethod pick-subcomponent ((p project) split-path)
  (let ((first (the simple-string (first split-path))))
    (cond ((string= first "tree")
	   (slot-value p 'tree))
	  ((string= first "commits")
	   (slot-value p 'commits)))))

(defmethod list-directory ((tree project-tree) project split-path)
  (loop
    (with-mutex ((fetch-status-lock-1 project))
      (when (>= (fetch-status project) *branches-fetched*)
	(return))
      (condition-wait (fetch-status-waitqueue-1 project) (fetch-status-lock-1 project))))

  (unless split-path
    (return-from list-directory
      (mapcar #'url-encode (project-heads project))))

  (let ((branch (url-decode (first split-path))))
    (prepare-checkout tree project branch)

    (if (>= (fetch-status project) *first-pack-fetched*)
	(list-tree-from-storage tree project branch (rest split-path))
	(list-tree-from-gitlab-api tree project branch (rest split-path)))))

(defmethod list-tree-from-storage ((tree project-tree) project branch split-path)
  (loop
    (with-mutex ((lock tree))
      (when (checked-out tree)
	(return))
      (condition-wait (queue tree) (lock tree))))

  (append
   (mapcar
    (lambda (pathname)
      (if (pathname-name pathname)
	  (file-namestring pathname)
	  (first (last (pathname-directory pathname)))))
    (directory (merge-pathnames
		(make-pathname :directory (append '(:relative)
						  (if (string= (first split-path) ".git")
						      (rest split-path)
						      split-path))
			       :name :wild
			       :type :wild)
		(if (string= (first split-path) ".git")
		    (let ((p (git-file-path tree project nil)))
		      (format t "path: ~A~%" p)
		      p)
		    (branch-folder tree project branch)))))
   (unless split-path '(".git"))))

(defmethod checkout-folder ((tree project-tree) project branch)
  (let ((branch-folder (branch-folder tree project branch))
	(branch-folder-sentinel (branch-folder-sentinel tree project branch)))
    (unless (probe-file branch-folder)
      (safer-ensure-directories-exist branch-folder))
    (unless (probe-file branch-folder-sentinel)
      (run-program
       (format nil "cd ~a && GIT_DIR=../.git GIT_WORK_TREE=. git checkout ~a *"
	       (namestring branch-folder)
	       branch))
      (open branch-folder-sentinel :direction :probe :if-does-not-exist :create))))

(defmethod branch-folder ((tree project-tree) project branch)
  (xdg-data-home
   (make-pathname
    :directory `(:relative "labfuse"
			   ,(project-path-with-namespace project)
			   ,(url-encode branch)))))

(defmethod branch-folder-sentinel ((tree project-tree) project branch)
  (xdg-data-home
   (make-pathname
    :directory `(:relative "labfuse"
			   ,(project-path-with-namespace project))
    :name (url-encode branch)
    :type "sentinel")))

(defmethod list-tree-from-gitlab-api ((tree project-tree) project branch split-path))

(defmethod fetch-git-repository ((p project))
  "Fetch the repository small part by small part in order to make some
parts available, ending up with a full-fledged repository.

In order to do this, we need, in the following order:
1. List of branches
2. First Git pack (i.e. shallow clone)
3. Rest of the Git repository (i.e. unshallow the clone)

Whilst this is running, the server will offer other ways to display
the results (i.e.. it will use the Gitlab API to return results
instead of relying on the local Git repository)."
  (with-mutex ((fetch-status-lock-1 p))
    (fetch-git-branches p)
    (with-mutex ((lock p)) (incf (fetch-status p)))
    (condition-broadcast (fetch-status-waitqueue-1 p)))

  (with-mutex ((fetch-status-lock-2 p))
    (fetch-git-first-pack p)
    (with-mutex ((lock p)) (incf (fetch-status p)))
    (condition-broadcast (fetch-status-waitqueue-2 p)))

  (with-mutex ((fetch-status-lock-3 p))
    (fetch-git-all-packs p)
    (with-mutex ((lock p)) (incf (fetch-status p)))
    (condition-broadcast (fetch-status-waitqueue-3 p))))

(defmethod fetch-git-branches ((p project))
  (let* ((lines (split #\Newline (run-program
                                  (format nil "git ls-remote --heads ~a"
                                          (render-uri
                                           (gitlab-project-clone-uri
					    (project-http-url-to-repo p))))
                                  :output '(:string :stripped t)))))
    (dolist (line lines)
      (let* ((ref (second (split #\Tab line)))
             (head (subseq ref (length "refs/heads/"))))
	(push head (project-heads p))))))

(defmethod fetch-git-first-pack ((p project))
  (let ((git-folder (merge-pathnames #p".git/" (storage-path p))))
    (unless (probe-file git-folder)
      ;; TODO: there's a fundamental flaw there, which is that we
      ;; don't actually know which branch will be visited first... so
      ;; it possibly makes sense to either do the full checkout right
      ;; away and rely on gitlab API in the meantime, or... something
      ;; else. To be defined.
      (run-program `("git"
		     "clone"
		     "--bare"
		     "--depth=1"
		     ,(render-uri (gitlab-project-clone-uri (project-http-url-to-repo p)))
		     ,(namestring git-folder))))))

(defmethod fetch-git-all-packs ((p project))
  ;; We have to do that in a shell, because run-program does not
  ;; support setting the cwd between fork and exec, which means we'd
  ;; have to maintain a global lock to temporarily chdir, and this
  ;; means that all the "git fetch --unshallow" operations would have
  ;; to be serialized.
  (let* ((git-folder (merge-pathnames #p".git/" (storage-path p)))
	 (sentinel-complete (merge-pathnames "labfuse.full" git-folder)))
    (unless (probe-file sentinel-complete)
      (run-program (format nil "cd ~a && git fetch --unshallow"
			   (namestring git-folder)))
      (open sentinel-complete :direction :probe :if-does-not-exist :create))))

(defmethod storage-path ((p project))
  (xdg-data-home (make-pathname
		  :directory `(:relative "labfuse" ,(project-path-with-namespace p)))))

(defmethod project-directoryp ((p project) split-path)
  (when-let ((subcomponent (pick-subcomponent p split-path)))
    (project-is-directory subcomponent p (rest split-path))))

(defmethod project-is-directory ((commits project-commits) project split-path)
  t)

(defmethod project-is-directory ((tree project-tree) project split-path)
  (cond
    ((null split-path) (return-from project-is-directory t))
    ((= (length split-path) 1)
     (return-from project-is-directory (member (first split-path)
					       (project-heads project)
					       :test #'equal))))

  (let ((branch (url-decode (first split-path))))
    (prepare-checkout tree project branch)
 
    (if (>= (fetch-status project) *first-pack-fetched*)
	(directoryp-from-storage tree project branch (rest split-path))
	(directoryp-from-gitlab-api tree project branch (rest split-path)))))

(defmethod directoryp-from-gitlab-api ((tree project-tree) project branch split-path)
  t)

(defmethod directoryp-from-storage ((tree project-tree) project branch split-path)
  (or (let ((path (probe-file (file-path tree project branch split-path))))
	(and path (not (pathname-name path))))
      (and (string= (first split-path) ".git")
	   (or (null (rest split-path))
	       (not (pathname-name (probe-file (git-file-path tree project (rest split-path)))))))))

(defmethod git-file-path ((tree project-tree) project split-path)
  (format nil "~a~{~a~^/~}"
	  (merge-pathnames #p".git/" (storage-path project))
	  split-path))

(defmethod prepare-checkout ((tree project-tree) project branch)
  (when (>= (fetch-status project) *first-pack-fetched*)
    (with-mutex ((lock tree))
      (unless (checked-out tree)
	(if (slot-boundp tree 'thread)
	    (condition-wait (queue tree) (lock tree))
	    (setf (thread tree)
		  (make-thread (lambda (tree project branch)
				 (with-mutex ((lock tree))
				   (checkout-folder tree project branch)
				   (setf (checked-out tree) t)
				   (condition-broadcast (queue tree))))
			       :arguments (list tree project branch))))))))

(defmethod file-path ((tree project-tree) project branch split-path)
  (format nil "~a~{~a~^/~}"
	  (branch-folder tree project branch)
	  split-path))

(defmacro file-methods (method args &key tree-storage tree-gitlab-api)
  (let ((project-method (intern (format nil "PROJECT-~a" method)))
	(subcomponent-method (intern (format nil "SUBCOMPONENT-~a" method)))
	(subcomponent-from-storage (intern (format nil "SUBCOMPONENT-~a-FROM-STORAGE" method)))
	(subcomponent-from-gitlab-api (intern (format nil "SUBCOMPONENT-~a-FROM-GITLAB-API" method))))
    `(progn
       (defmethod ,project-method ((p project) split-path ,@args)
	 (when-let ((subcomponent (pick-subcomponent p split-path)))
	   (,subcomponent-method subcomponent p (rest split-path) ,@args)))

       (defmethod ,subcomponent-method ((tree project-tree) project split-path ,@args)
	 (when (or (= (length split-path) 0)
		   (not (member (first split-path) (project-heads project) :test #'equal)))
	   (return-from ,subcomponent-method))

	 (let ((branch (url-decode (first split-path))))
	   (prepare-checkout tree project branch)

	   (if (>= (fetch-status project) *first-pack-fetched*)
	       (,subcomponent-from-storage tree project branch (rest split-path) ,@args)
	       (,subcomponent-from-gitlab-api tree project branch (rest split-path) ,@args))))

       (defmethod ,subcomponent-from-storage ((tree project-tree) project branch split-path ,@args)
	 ,tree-storage)

       (defmethod ,subcomponent-from-gitlab-api ((tree project-tree) project branch split-path ,@args)
	 ,tree-gitlab-api))))

(file-methods file-open (flags)
  :tree-storage
  (sb-posix:open (file-path tree project branch split-path)
		 flags))

(file-methods file-release (fh)
  :tree-storage
  (progn
    (sb-posix:close fh)
    t))

(file-methods file-size ()
  :tree-storage
  (handler-case
      (let ((stat (sb-posix:stat (file-path tree project branch split-path))))
	(sb-posix:stat-size stat))
    (error () nil)))

(file-methods file-read (size offset fh)
  :tree-storage
  (handler-case
      (progn
	;; todo: technically we should lock between lseek and read
	(sb-posix:lseek fh offset sb-posix:seek-set)
	(let ((buffer (make-array size :element-type '(unsigned-byte 8))))
	  (sb-posix:read fh (sb-sys:vector-sap buffer) size)
	  buffer))
    (error () nil)))

(file-methods file-write (data offset fh)
  :tree-storage
  (handler-case
      (progn
	;; todo: technically we should lock between lseek and write
	(sb-posix:lseek fh offset sb-posix:seek-set)
	(sb-posix:write fh data (length data))
	t)
    (error () nil)))

(file-methods symlink-target ()
  :tree-storage
  (handler-case
      (sb-posix:readlink (file-path tree project branch split-path))
    (error () nil)))

(file-methods symlinkp ()
  :tree-storage
  (handler-case
      (let ((stat (sb-posix:lstat (file-path tree project branch split-path))))
	(sb-posix:s-islnk (sb-posix:stat-mode stat)))
    (error () nil)))

(file-methods file-writeable-p ()
  :tree-storage
  (handler-case
      (let ((stat (sb-posix:stat (file-path tree project branch split-path))))
	(> (logand (sb-posix:stat-mode stat) sb-posix:s-iwusr)
	   0))
    (error () nil)))

(file-methods file-executable-p ()
  :tree-storage
  (handler-case
      (let ((stat (sb-posix:stat (file-path tree project branch split-path))))
	(> (logand (sb-posix:stat-mode stat) sb-posix:s-ixusr)
	   0))
    (error () nil)))
